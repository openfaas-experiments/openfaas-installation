BOX_IMAGE = "bento/ubuntu-16.04"
VERSION = "1.0"

REGISTRY_NAME = "my-registry"
REGISTRY_IP = "172.16.245.160"
REGISTRY_DOMAIN = "registry.test"
REGISTRY_PORT = 5000
REGISTRY = "registry.test:#{REGISTRY_PORT}"

OPENFAAS_NAME = "my-openfaas"
OPENFAAS_IP = "172.16.245.201"

Vagrant.configure("2") do |config|
  config.vm.box = BOX_IMAGE
  ENV['LC_ALL']="en_US.UTF-8"

  # === DOCKER REGISTRY ===
  config.vm.define "#{REGISTRY_NAME}" do |node|

    node.vm.hostname = "#{REGISTRY_NAME}"    
    node.vm.network "private_network", ip: "#{REGISTRY_IP}"

    node.vm.provider "virtualbox" do |vb|
      vb.memory = 512
      vb.cpus = 1
      vb.name = "#{REGISTRY_NAME}"
    end

    node.vm.provision :shell, inline: <<-SHELL
      apt-get update
      # === docker install ===
      curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
      apt-key fingerprint 0EBFCD88
      add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
      apt-get update
      apt-get install -y docker-ce
      # === run the registry ===
      docker run -d -p #{REGISTRY_PORT}:#{REGISTRY_PORT} --restart=always --name registry registry:2
    SHELL

  end

  # === OpenFaaS ===
  config.vm.define "#{OPENFAAS_NAME}" do |node|

    node.vm.hostname = "#{OPENFAAS_NAME}"
    node.vm.network "private_network", ip: "#{OPENFAAS_IP}"

    node.vm.provider "virtualbox" do |vb|
      vb.memory = 1024
      vb.cpus = 1
      vb.name = "#{OPENFAAS_NAME}"
    end

    node.vm.provision :shell, inline: <<-SHELL

      apt-get update
      # === docker install ===
      curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
      apt-key fingerprint 0EBFCD88
      add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
      apt-get update
      apt-get install -y docker-ce

      # Add entries to hosts file:
      echo "" >> /etc/hosts
      echo '#{REGISTRY_IP} #{REGISTRY_DOMAIN}' >> /etc/hosts 
      echo "" >> /etc/hosts

      docker swarm init --advertise-addr #{OPENFAAS_IP}

      # Add unsecure registry
      echo "" >> /etc/docker/daemon.json
      echo '{' >> /etc/docker/daemon.json
      echo '  "insecure-registries" : ["#{REGISTRY}"]' >> /etc/docker/daemon.json
      echo '}' >> /etc/docker/daemon.json
      echo "" >> /etc/docker/daemon.json

      service docker restart

      # === OpenFaaS install ===
      git clone https://github.com/openfaas/faas && \
      cd faas && \
      ./deploy_stack.sh

    SHELL

    node.vm.provision :shell, run: "always", inline: <<-SHELL
      sudo usermod -a -G docker $USER
      sudo chmod 666 /var/run/docker.sock      
    SHELL

  end


end


